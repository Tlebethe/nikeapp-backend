const express = require('express');
const app = express();
require('dotenv').config()
const productRoute = require('./routes/productRoutes')
const ordersRoute = require('./routes/ordersRoutes')
var bodyParser = require('body-parser')
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
 
const PORT = process.env.PORT || 3000

app.use('/products', productRoute)
app.use('/orders', ordersRoute)

app.get('/', (req, res)=> {
    res.send('hello world');
})


app.listen(PORT, ()=>{
    console.log('Server is listening on port: ', PORT)
})