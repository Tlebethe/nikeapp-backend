const express = require('express')
const router = express.Router()
const orders = require('../database/orders')


router.get('/:refId', async(req, res)=> {
    try {
        console.log(`get order by ref ${req.params.refId}`)
        const refId = req.params.refId;
        const data = await orders.getOrderByRef(refId);
        res.json({status: 'OK', data})
        
    } catch (error) {
        console.log('error occured...')
        res.json({error})
        
    }

})

router.get('/',async (req, res)=> {
    try {
        console.log('getting orders')
        const data = await orders.getOrders()
        res.json({status: 'OK', data})
        
    } catch (error) {
        console.log('error happened')
        res.json({error})
    }

})

router.post('/',async (req, res)=> {
    try {
        console.log('creating order...')
        const orderPost = req.body;
        const data = await orders.createOrder(orderPost);
        console.log('order created successfully...')
        res.json({status: 'posted', data})
    } catch (error) {
        console.log('error: ', error)
        res.json({error})
    }

})

module.exports = router;


