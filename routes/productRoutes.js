const express = require('express')
const router = express.Router();
const {getAllProducts, getProductById } = require('../database/products')


router.get('/', async(req, res)=> {
    try {
        const data = await getAllProducts()
        console.log('fetching data...')
        res.json({status: 'OK', data: data})
        
    } catch (error) {
        res.json({error})
    }

})

router.get('/:id',async (req, res)=> {
    const id = req.params.id
    try {
        const data = await getProductById(id)
        console.log(`getting shoe with id ${id}`)
        res.json({status: 'ok', data: data})
        return
    } catch (error) {
        res.status(404).json({error: error.message})
        return
    }
})

module.exports = router;