const db = require('./connect')

const getOrders = async()=> {
    const data = await db.orders.find().toArray()
    return data
}

const getOrderByRef = async (refId)=> {
    const data = await db.orders.findOne({refId: refId});
    return data;
}

const createOrder = async (orderObj) => {
    const result = Math.random().toString(36).substring(2,7);
    orderObj.refId = result;
    const data = await db.orders.insertOne({orderObj});
    return {
        data,
        result
    };
}

module.exports = {
    getOrders,
    getOrderByRef,
    createOrder,
}

